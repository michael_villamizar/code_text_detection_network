## Installation

#### Check Prerequisites

Check out your python and pip installation:

````
python --version
````

````
pip --version
````

#### Create virtual environment

Install virtualenv via pip:

````
pip install virtualenv
````

Test your installation:

````
virtualenv --version
````

Create virtual environment named env_textnet_gpu (python 3):

````
virtualenv -p python3 env_textnet_gpu
````

Activate virtual environment:

````
source ./env_textnet_gpu/bin/activate
````

#### Install requirements

Install all dependencies via pip:

````
pip install -r requirements_gpu.txt
````
