## Installation

#### Check Prerequisites

Check out your python and pip installation:

````
python --version
````

````
pip --version
````

#### Create virtual environment

Install virtualenv via pip:

````
pip install virtualenv
````

Test your installation:

````
virtualenv --version
````

Create virtual environment named env_textnet_cpu (python 3):

````
virtualenv -p python3 env_textnet_cpu
````

Activate virtual environment:

````
source ./env_textnet_cpu/bin/activate
````

#### Install requirements

Install all dependencies via pip:

````
pip install -r requirements_cpu.txt
````
