import numpy as np
from PIL import Image, ImageDraw, ImageFont, ImageOps
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

# Compute bounding boxes.
def bounding_boxes(input_array, cell_size):
    ''' This function extracts the bounding boxes from the input array. This
    array could correspond to the network output -text detections- or the
    ground truth -text masks-. The input array is a cell grid containing the
    confidence and bounding boxes of text lines (e.g words). Boxes are provided
    in format [x0, y0, x1, y1].'''

    # Copy input array to not change input values.
    input_array = np.copy(input_array)

    # Input array size.
    size_y = np.shape(input_array)[1]  # Size y.
    size_x = np.shape(input_array)[2]  # Size x.
    num_imgs = np.shape(input_array)[0]  # Number of images.
    num_feats = np.shape(input_array)[3]  # Number of features -confidence and
                                          # box coordinates-.
    # Adapt box coordinates according to the cell size.
    input_array[:, :, :, 1] *= cell_size[0]
    input_array[:, :, :, 2] *= cell_size[1]
    input_array[:, :, :, 3] *= cell_size[0]
    input_array[:, :, :, 4] *= cell_size[1]

    # Adapt box coordinates to the image -absolute location-.
    input_array[:, :, :, 1] += 0.5*cell_size[0]
    input_array[:, :, :, 2] += 0.5*cell_size[1]
    input_array[:, :, :, 3] += 0.5*cell_size[0]
    input_array[:, :, :, 4] += 0.5*cell_size[1]

    # Adapt box coordinates to the image -absolute location-.
    loc_cell_x = np.zeros((size_y, size_x))
    loc_cell_y = np.zeros((size_y, size_x))
    for y in range(size_y):
        for x in range(size_x):
            loc_cell_x[y, x] = x*cell_size[0]
            loc_cell_y[y, x] = y*cell_size[1]
    for n in range(num_imgs):
        input_array[n, :, :, 1] += loc_cell_x
        input_array[n, :, :, 2] += loc_cell_y
        input_array[n, :, :, 3] += loc_cell_x
        input_array[n, :, :, 4] += loc_cell_y

    # Reshape -cells are removed-.
    boxes = input_array.reshape((num_imgs, size_x*size_y, num_feats))

    return boxes

# Bounding box values.
def bounding_box_values(box):
    ''' This function returns the bounding box attributes: the detection
    confidence, and the left, top, right and bottom locations.'''

    # Assert.
    assert box.ndim==1

    # Box values.
    x0 = box[1]  # Left value.
    y0 = box[2]  # Top value.
    x1 = box[3]  # Right value.
    y1 = box[4]  # Bottom value.
    conf = box[0] # Confidence.

    return conf, x0, y0, x1, y1

# Thresholding boxes.
def thresholding(boxes, threshold):
    ''' This function thresholds boxes using the score -confidence- associated
    to each box and the input threshold.'''

    # Copy input array to not change input values.
    boxes = np.copy(boxes)

    # Check empty boxes.
    if np.shape(boxes)[0]==0: return []

    # Threshold indexes -confidence values are in the first column-.
    indx = boxes[:, 0]>=threshold

    # Thresholding boxes.
    thr_boxes = boxes[indx, :]

    return thr_boxes

#  Non-maxima suppression.
def non_maxima_suppression(boxes, threshold):
    ''' This function performs non-maxima suppression using the scores
    associated to the boxes and the input overlapping threshold. '''

    # Copy input array to not change input values.
    boxes = np.copy(boxes)

    # If there are no boxes, return an empty list.
    if np.shape(boxes)[0]==0: return []

    # Initialize the list of picked indexes.
    pick = []

    # Grab the coordinates of the bounding boxes.
    x1 = boxes[:,1]
    y1 = boxes[:,2]
    x2 = boxes[:,3]
    y2 = boxes[:,4]

    # Scores associated to boxes.
    scores = boxes[:, 0]

    # Compute the area of the bounding boxes and sort the bounding boxes by the
    # input scores associated to each bounding box.
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(scores)

    # Keep looping while some indexes still remain in the indexes list.
    while len(idxs) > 0:

        # Grab the last index in the indexes list, add the index value to the
        # list of picked indexes, then initialize the suppression list (i.e.
        # indexes that will be deleted) using the last index.
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        suppress = [last]

        # Loop over all indexes in the indexes list.
        for pos in range(0, last):

            # Grab the current index.
            j = idxs[pos]

            # Find the largest (x, y) coordinates for the start of the bounding
            # box and the smallest (x, y) coordinates for the end of the
            # bounding box.
            xx1 = max(x1[i], x1[j])
            yy1 = max(y1[i], y1[j])
            xx2 = min(x2[i], x2[j])
            yy2 = min(y2[i], y2[j])

            # Compute the width and height of the bounding box.
            w = max(0, xx2 - xx1 + 1)
            h = max(0, yy2 - yy1 + 1)

            # Compute the ratio of overlap between the computed bounding box
            # and the bounding box in the area list.
            overlap = float(w * h) / area[j]

            # If there is sufficient overlap, suppress the current bounding
            # box.
            if overlap > threshold: suppress.append(pos)

        # Delete all indexes from the index list that are in the suppression
        # list.
        idxs = np.delete(idxs, suppress)

    # Return only the bounding boxes that were picked.
    return boxes[pick]

# Intersection over Union (IoU).
def intersection_over_union(boxes_a, boxes_b):
    ''' This function computes the intersection over union between boxes A and
    boxes B. The function returns an array between boxes A and B.'''

    # Numbers of boxes.
    num_boxes_a = np.shape(boxes_a)[0]
    num_boxes_b = np.shape(boxes_b)[0]

    # Intersection over union.
    iou = np.zeros((num_boxes_a, num_boxes_b))

    # Boxes A.
    for a in range(num_boxes_a):

        # Box A values.
        _, xa0, ya0, xa1, ya1 = bounding_box_values(boxes_a[a, :])

        # Boxes B.
        for b in range(num_boxes_b):

            # Box B values.
            _, xb0, yb0, xb1, yb1 = bounding_box_values(boxes_b[b, :])

            # Intersection area.
            x0 = np.maximum(xa0, xb0)
            y0 = np.maximum(ya0, yb0)
            x1 = np.minimum(xa1, xb1)
            y1 = np.minimum(ya1, yb1)
            inter_area = (x1-x0+1)*(y1-y0+1)

            # Boxes areas.
            area_a = (xa1-xa0+1)*(ya1-ya0+1)
            area_b = (xb1-xb0+1)*(yb1-yb0+1)

            # Intersection over union (IoU).
            iou[a, b] = float(inter_area)/float(area_a + area_b - inter_area)

            # Check no intersection
            if (x1-x0+1)<0: iou[a, b] = 0.0
            if (y1-y0+1)<0: iou[a, b] = 0.0

    return iou

# Recall, precision and f-measure rates.
def recall_precision_scores(tps=1.0, fps=1.0, fns=1.0):
    ''' This function computes the recall, precision and f-measure rates using
    the input numbers of true positives, false positives and false
    negatives.'''

    # Variables.
    rec = 0.0  # Recall.
    pre = 0.0  # Precision.
    fme = 0.0  # F-measure.

    # Assert -positive values-.
    assert tps>=0
    assert fps>=0
    assert fns>=0

    # Computation.
    if tps>0:
        rec = float(tps)/float(tps + fns)
        pre = float(tps)/float(tps + fps)
        fme = float(2*rec*pre)/float(rec+pre)

    return rec, pre, fme

# Compute detection rates.
def detection_rates(detection_boxes, dataset_boxes, ovlp_threshold):
    ''' This function computes detection rates over the predictions -detection
    boxes- using the ground truth of the dataset -dataset boxes-. The detection
    rates are computed according to the input overlapping threshold.'''

    # Copy input arrays to not change input values.
    dataset_boxes = np.copy(dataset_boxes)
    detection_boxes = np.copy(detection_boxes)

    # Number of detection and dataset boxes.
    num_dataset_boxes = np.shape(dataset_boxes)[0]
    num_detection_boxes = np.shape(detection_boxes)[0]

    # Check number of detections.
    if num_detection_boxes==0:
        # Return not detection values.
        return 0, 0, num_dataset_boxes, 0, 0, 0

    # Intersection over Union -please keep the order of arguments-.
    iou = intersection_over_union(detection_boxes, dataset_boxes)

    # Maximum IoU.
    indx = np.argmax(iou, axis=0)
    max_iou = np.zeros_like(iou)
    max_iou[indx, range(np.shape(iou)[1])] = 1.0
    max_iou = max_iou*iou

    # Overlapping threshold.
    thr_iou = np.copy(max_iou)
    thr_iou[max_iou<ovlp_threshold] = 0.0
    thr_iou[max_iou>=ovlp_threshold] = 1.0

    # Detection rates.
    tmp =  np.sum(thr_iou, axis=0)
    tmp =  np.minimum(tmp, 1)
    tps = np.sum(tmp)  # True positives.
    fns = num_dataset_boxes - tps  # False negatives.
    fps = num_detection_boxes - tps  # False positives.

    # Recall, precision and f-score rates.
    rec, pre, fme = recall_precision_scores(tps, fps, fns)

    return tps, fps, fns, rec, pre, fme

# Evaluation rates.
def evaluation_rates(detection_boxes, dataset_boxes, det_ovlp_threshold=0.3,\
                     eva_ovlp_threshold=0.5, det_threshold=0.5):
    ''' This functions evaluates the performance of the network for text
    detection.  The evaluation is done using the output of the network
    -detection boxes-  and the ground truth of the dataset -dataset boxes-.
    The function returns the detection rates for different thresholds, between
    the minimum and maximum detection thresholds.. The detection rates are
    computed according to the detection and evaluation overlapping thresholds.
    The former is to remove multiple text detections whereas the latter is to
    measure the accuracy of the detections.'''

    # Copy input arrays to not change input values.
    dataset_boxes = np.copy(dataset_boxes)
    detection_boxes = np.copy(detection_boxes)

    # Number of dataset and detection images.
    num_dataset_imgs = np.shape(dataset_boxes)[0]
    num_detection_imgs = np.shape(detection_boxes)[0]

    # Assert -they must have the same number of images-.
    assert num_dataset_imgs == num_detection_imgs

    # Variables.
    acc_tps = 0  # Accumulative true positives.
    acc_fps = 0  # Accumulative false positives.
    acc_fns = 0  # Accumulative false negatives.

    # Images.
    for n in range(num_dataset_imgs):

        # Boxes for current image.
        dat_boxes = dataset_boxes[n, :, :]
        det_boxes = detection_boxes[n, :, :]

        # Thresholded the detection boxes.
        thr_det_boxes = thresholding(det_boxes, det_threshold)

        # Keep text bounding boxes only (confidence = 1.0).
        thr_dat_boxes = thresholding(dat_boxes, 1.0)

        # Non-maxima suppression.
        max_det_boxes = non_maxima_suppression(thr_det_boxes, det_ovlp_threshold)

        # Compute detection rates.
        tps, fps, fns, _, _, _ = detection_rates(max_det_boxes, thr_dat_boxes, \
                                                 eva_ovlp_threshold)
        # Update rates.
        acc_tps += tps  # True positives.
        acc_fps += fps  # False positives.
        acc_fns += fns  # False negatives.

    # Compute recall, precision and f-measure rates.
    rec, pre, fme = recall_precision_scores(acc_tps, acc_fps, acc_fns)

    # Save current rates.
    rates = [acc_tps, acc_fps, acc_fns, rec, pre, fme]

    return rates

# Text detection accuracy.
def text_detection_accuracy(network_output, annotation_masks, cell_size, \
                            det_thr=0.5, det_ovlp_thr=0.3, eva_ovlp_thr=0.5):
    ''' This function computes the accuracy of the network for text detection
    using bounding boxes. The accuracy is computed comparing the dataset
    bounding boxes against the network output -text bounding boxes-'''

    # Compute dataset and detection boxes.
    dataset_boxes = bounding_boxes(annotation_masks, cell_size)
    detection_boxes = bounding_boxes(network_output, cell_size)

    # Compute evaluation rates.
    rates = evaluation_rates(detection_boxes, dataset_boxes, det_ovlp_thr, \
                             eva_ovlp_thr, det_thr)
    return rates

# Draw bounding boxes.
def draw_bounding_boxes(img, boxes, box_color=(255, 0, 0)):
    ''' This function draws bounding boxes in images.'''

    # Number of boxes.
    num_boxes = np.shape(boxes)[0]

    # Boxes.
    for b in range(num_boxes):

        # Box values.
        conf, x0, y0, x1, y1 = bounding_box_values(boxes[b, :])

        # Draw box.
        img.rectangle([x0, y0, x1, y1], None, box_color)

# Draw text boxes.
def draw_text_boxes(imgs, network_output, cell_size, det_thr=0.5, \
                    det_ovlp_thr=0.3):
    ''' This function draws bounding boxes around text in images. '''

    # Number of images.
    num_imgs = imgs.shape[0]

    # Image size.
    sy = imgs.shape[1]
    sx = imgs.shape[2]
    nc = imgs.shape[3]

    # Compute dataset and detection boxes.
    det_boxes = bounding_boxes(network_output, cell_size)

    # New images -with bounding boxes-.
    new_imgs = np.zeros((num_imgs, sy, sx, nc))

    # Draw bounding boxes.
    for i in range(num_imgs):

        # Thresholded the detection boxes.
        thr_det_boxes = thresholding(det_boxes[i,:,:], det_thr)

        # Non-maxima suppression.
        max_det_boxes = non_maxima_suppression(thr_det_boxes, det_ovlp_thr)

        # Convert to PIL image.
        img = Image.fromarray(np.uint8(imgs[i,:,:,:])).convert('RGB')

        # Draw bounding box.
        draw = ImageDraw.Draw(img)
        draw = draw_bounding_boxes(draw, max_det_boxes)

        # Save new image.
        new_imgs[i,:,:,:] = np.array(img)

    return new_imgs
